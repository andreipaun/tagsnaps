#Const x64 = False
Option Strict Off
Option Explicit On
Imports System.Runtime.InteropServices

Module CxStat
    '
    Public Const GROUP_USUALLY As Integer = &H10000
    Public Const GROUP_SETTING As Integer = &H20000
    Public Const GROUP_HARDWARE As Integer = &H40000
    Public Const GROUP_SYSTEM As Integer = &H80000
    Public Const GROUP_FLSHPROG As Integer = &H100000

    Public Const STATUS_ERROR As Integer = &H80000000

    Public Const STATUS_USUALLY_IDLE As Integer = GROUP_USUALLY Or &H1S
    Public Const STATUS_USUALLY_PRINTING As Integer = GROUP_USUALLY Or &H2S
    Public Const STATUS_USUALLY_STANDSTILL As Integer = GROUP_USUALLY Or &H4S
    Public Const STATUS_USUALLY_PAPER_END As Integer = GROUP_USUALLY Or &H8S
    Public Const STATUS_USUALLY_RIBBON_END As Integer = GROUP_USUALLY Or &H10S
    Public Const STATUS_USUALLY_COOLING As Integer = GROUP_USUALLY Or &H20S
    Public Const STATUS_USUALLY_MOTCOOLING As Integer = GROUP_USUALLY Or &H40S

    Public Const STATUS_SETTING_COVER_OPEN As Integer = GROUP_SETTING Or &H1S
    Public Const STATUS_SETTING_PAPER_JAM As Integer = GROUP_SETTING Or &H2S
    Public Const STATUS_SETTING_RIBBON_ERR As Integer = GROUP_SETTING Or &H4S
    Public Const STATUS_SETTING_PAPER_ERR As Integer = GROUP_SETTING Or &H8S
    Public Const STATUS_SETTING_DATA_ERR As Integer = GROUP_SETTING Or &H10S
    Public Const STATUS_SETTING_SCRAPBOX_ERR As Integer = GROUP_SETTING Or &H20S

    Public Const STATUS_HARDWARE_ERR01 As Integer = GROUP_HARDWARE Or &H1S
    Public Const STATUS_HARDWARE_ERR02 As Integer = GROUP_HARDWARE Or &H2S
    Public Const STATUS_HARDWARE_ERR03 As Integer = GROUP_HARDWARE Or &H4S
    Public Const STATUS_HARDWARE_ERR04 As Integer = GROUP_HARDWARE Or &H8S
    Public Const STATUS_HARDWARE_ERR05 As Integer = GROUP_HARDWARE Or &H10S
    Public Const STATUS_HARDWARE_ERR06 As Integer = GROUP_HARDWARE Or &H20S
    Public Const STATUS_HARDWARE_ERR07 As Integer = GROUP_HARDWARE Or &H40S
    Public Const STATUS_HARDWARE_ERR08 As Integer = GROUP_HARDWARE Or &H80S
    Public Const STATUS_HARDWARE_ERR09 As Integer = GROUP_HARDWARE Or &H100S
    Public Const STATUS_HARDWARE_ERR10 As Integer = GROUP_HARDWARE Or &H200S

    Public Const STATUS_SYSTEM_ERR01 As Integer = GROUP_SYSTEM Or &H1S

    Public Const STATUS_FLSHPROG_IDLE As Integer = GROUP_FLSHPROG Or &H1S
    Public Const STATUS_FLSHPROG_WRITING As Integer = GROUP_FLSHPROG Or &H2S
    Public Const STATUS_FLSHPROG_FINISHED As Integer = GROUP_FLSHPROG Or &H4S
    Public Const STATUS_FLSHPROG_DATA_ERR1 As Integer = GROUP_FLSHPROG Or &H8S
    Public Const STATUS_FLSHPROG_DEVICE_ERR1 As Integer = GROUP_FLSHPROG Or &H10S
    Public Const STATUS_FLSHPROG_OTHERS_ERR1 As Integer = GROUP_FLSHPROG Or &H20S

    Public Const CUTTER_MODE_STANDARD As Short = &H0S
    Public Const CUTTER_MODE_NONSCRAP As Short = &H1S
    Public Const CUTTER_MODE_2INCHCUT As Short = &H78S

    'For CV Printers Item
    Public Const CVG_USUALLY As Integer = &H10000
    Public Const CVG_SETTING As Integer = &H20000
    Public Const CVG_HARDWARE As Integer = &H40000
    Public Const CVG_SYSTEM As Integer = &H80000
    Public Const CVG_FLSHPROG As Integer = &H100000

    Public Const CVSTATUS_ERROR As Integer = &H80000000

    Public Const CVS_USUALLY_IDLE As Integer = CVG_USUALLY Or &H1S
    Public Const CVS_USUALLY_PRINTING As Integer = CVG_USUALLY Or &H2S
    Public Const CVS_USUALLY_STANDSTILL As Integer = CVG_USUALLY Or &H4S
    Public Const CVS_USUALLY_PAPER_END As Integer = CVG_USUALLY Or &H8S
    Public Const CVS_USUALLY_RIBBON_END As Integer = CVG_USUALLY Or &H10S
    Public Const CVS_USUALLY_COOLING As Integer = CVG_USUALLY Or &H20S
    Public Const CVS_USUALLY_MOTCOOLING As Integer = CVG_USUALLY Or &H40S

    Public Const CVS_SETTING_COVER_OPEN As Integer = CVG_SETTING Or &H1S
    Public Const CVS_SETTING_PAPER_JAM As Integer = CVG_SETTING Or &H2S
    Public Const CVS_SETTING_RIBBON_ERR As Integer = CVG_SETTING Or &H4S
    Public Const CVS_SETTING_PAPER_ERR As Integer = CVG_SETTING Or &H8S
    Public Const CVS_SETTING_DATA_ERR As Integer = CVG_SETTING Or &H10S
    Public Const CVS_SETTING_SCRAP_ERR As Integer = CVG_SETTING Or &H20S

    Public Const CVS_HARDWARE_ERR01 As Integer = CVG_HARDWARE Or &H1S
    Public Const CVS_HARDWARE_ERR02 As Integer = CVG_HARDWARE Or &H2S
    Public Const CVS_HARDWARE_ERR03 As Integer = CVG_HARDWARE Or &H4S
    Public Const CVS_HARDWARE_ERR04 As Integer = CVG_HARDWARE Or &H8S
    Public Const CVS_HARDWARE_ERR05 As Integer = CVG_HARDWARE Or &H10S
    Public Const CVS_HARDWARE_ERR06 As Integer = CVG_HARDWARE Or &H20S
    Public Const CVS_HARDWARE_ERR07 As Integer = CVG_HARDWARE Or &H40S
    Public Const CVS_HARDWARE_ERR08 As Integer = CVG_HARDWARE Or &H80S
    Public Const CVS_HARDWARE_ERR09 As Integer = CVG_HARDWARE Or &H100S
    Public Const CVS_HARDWARE_ERR10 As Integer = CVG_HARDWARE Or &H200S

    Public Const CVS_SYSTEM_ERR01 As Integer = CVG_SYSTEM Or &H1S

    Public Const CVS_FLSHPROG_IDLE As Integer = CVG_FLSHPROG Or &H1S
    Public Const CVS_FLSHPROG_WRITING As Integer = CVG_FLSHPROG Or &H2S
    Public Const CVS_FLSHPROG_FINISHED As Integer = CVG_FLSHPROG Or &H4S
    Public Const CVS_FLSHPROG_DATA_ERR1 As Integer = CVG_FLSHPROG Or &H8S
    Public Const CVS_FLSHPROG_DEVICE_ERR1 As Integer = CVG_FLSHPROG Or &H10S
    Public Const CVS_FLSHPROG_OTHERS_ERR1 As Integer = CVG_FLSHPROG Or &H20S

#If x64 Then

    Declare Function CvInitialize Lib "CxStat64.dll" (<MarshalAs(UnmanagedType.LPWStr)> ByVal pszPortName As String) As Integer
    Declare Function CvGetVersion Lib "CxStat64.dll" (ByVal lPortNum As integer, ByVal s As String) As integer
    Declare Function CvGetSensorInfo Lib "CxStat64.dll" (ByVal lPortNum As integer, ByVal s As String) As integer
    Declare Function CvGetMedia Lib "CxStat64.dll" (ByVal lPortNum As integer, ByVal s As String) As integer
    Declare Function CvGetStatus Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetPQTY Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetCounterL Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetCounterA Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetCounterB Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetMediaCounter Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetMediaColorOffset Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetMediaLotNo Lib "CxStat64.dll" (ByVal lPortNum As integer, ByVal s As String) As integer
    Declare Function CvSetColorDataVersion_ Lib "CxStat64.dll" Alias "CvSetColorDataVersion" (ByVal lPortNum As Integer, ByVal lpBuff As Long, ByVal bLen As Integer) As Integer
    Declare Function CvSetColorDataWrite_ Lib "CxStat64.dll" Alias "SetColorDataWrite" (ByVal lPortNum As Integer, ByVal lpBuff As Long, ByVal bLen As Integer) As Integer
    Declare Function CvGetColorDataVersion Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvSetColorDataClear Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetColorDataChecksum Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvGetSerialNo Lib "CxStat64.dll" (ByVal lPortNum As integer, ByVal s As String) As integer
    Declare Function CvGetResolutionH Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetResolutionV Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvGetFreeBuffer Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvSetClearCounterA Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvSetClearCounterB Lib "CxStat64.dll" (ByVal lPortNum As integer) As integer
    Declare Function CvSetFirmwUpdateMode Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvSetFirmwDataWrite_ Lib "CxStat64.dll" Alias "SetFirmwDataWrite" (ByVal lPortNum As Integer, ByVal lpBuff As Long, ByVal bLen As Integer) As Integer
    Declare Function CvSetCommand_ Lib "CxStat64.dll" Alias "CvSetCommand" (ByVal lPortNum As Integer, ByVal lpCmd As Long, ByVal dwCmdLen As Integer) As Integer
    Declare Function CvGetCommand_ Lib "CxStat64.dll" Alias "CvGetCommand" (ByVal lPortNum As Integer, ByVal lpCmd As Long, ByVal dwCmdLen As Integer, ByVal lpRetBuff As String, ByVal dwRetBuffSize As Integer, ByRef lpdwRetLen As Long) As Integer
    Declare Function CvGetCommandEX_ Lib "CxStat64.dll" Alias "CvGetCommandEX" (ByVal lPortNum As Integer, ByVal lpCmd As Long, ByVal dwCmdLen As Integer, ByVal lpRetBuff As String, ByVal dwRetBuffSize As Integer) As Integer
    '
    Declare Function PortInitialize Lib "CxStat64.dll" (<MarshalAs(UnmanagedType.LPWStr)> ByVal pszPortName As String) As Integer
    Declare Function GetFirmwVersion Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetSensorInfo Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetMedia Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetStatus Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetPQTY Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterL Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterA Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterB Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterP Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterMatte Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterM Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetMediaCounter Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetMediaCounter_R Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetMediaColorOffset Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetMediaLotNo Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetMediaIdSetInfo Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetColorDataVersion_ Lib "CxStat64.dll" Alias "SetColorDataVersion" (ByVal lPortNum As Integer, ByVal lpBuff As Long, ByVal bLen As Integer) As Integer
    Declare Function SetColorDataWrite_ Lib "CxStat64.dll" Alias "SetColorDataWrite" (ByVal lPortNum As Integer, ByVal lpBuff As Long, ByVal bLen As Integer) As Integer
    Declare Function GetColorDataVersion Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function SetColorDataClear Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetColorDataChecksum Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetSerialNo Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetResolutionH Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetResolutionV Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetFreeBuffer Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetClearCounterA Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetClearCounterB Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetClearCounterM Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetCounterP Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal Counter As Integer) As Integer
    Declare Function SetFirmwUpdateMode Lib "CxStat64.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetFirmwDataWrite_ Lib "CxStat64.dll" Alias "SetFirmwDataWrite" (ByVal lPortNum As Integer, ByVal lpBuff As Long, ByVal bLen As Integer) As Integer
    Declare Function SetCutterMode Lib "CxStat64.dll" (ByVal lPortNum As Integer, ByVal d As Integer) As Integer
    Declare Function SetCommand_ Lib "CxStat64.dll" Alias "SetCommand" (ByVal lPortNum As Integer, ByVal lpCmd As Long, ByVal dwCmdLen As Integer) As Integer
    Declare Function GetCommand_ Lib "CxStat64.dll" Alias "GetCommand" (ByVal lPortNum As Integer, ByVal lpCmd As Long, ByVal dwCmdLen As Integer, ByVal lpRetBuff As String, ByVal dwRetBuffSize As Integer, ByRef lpdwRetLen As Long) As Integer
    Declare Function GetCommandEX_ Lib "CxStat64.dll" Alias "GetCommandEX" (ByVal lPortNum As Integer, ByVal lpCmd As Long, ByVal dwCmdLen As Integer, ByVal lpRetBuff As String, ByVal dwRetBuffSize As Integer) As Integer


    '-----------------------------------------------------------------------------------API Wrapper function
    Public Function CvSetCommand( _
            ByVal lPortNum As Integer, _
            ByVal Cmd As String, _
            ByVal dwCmdLen As Integer _
            ) As Integer

        Dim ByteArray As Byte()
        'Code Page 65001 = UTF-8
        ByteArray = System.Text.Encoding.GetEncoding(65001).GetBytes(Cmd)
        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Long = gchCmd.AddrOfPinnedObject().ToInt64()
        CvSetCommand = CvSetCommand_(lPortNum, lpCmd, dwCmdLen)

        gchCmd.Free()

    End Function


    Public Function CvGetCommand( _
                    ByVal lPortNum As Integer, _
                    ByVal Cmd As String, _
                    ByVal dwCmdLen As Integer, _
                    ByRef rb As String, _
                    ByVal dwRetBuffSize As Integer, _
                    ByRef dwRetLen As Integer _
                    ) As Integer

        Dim ByteArray As Byte()
        ByteArray = System.Text.Encoding.GetEncoding(65001).GetBytes(Cmd)
        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim gchRetLen As GCHandle = GCHandle.Alloc(dwRetLen, GCHandleType.Pinned)
        Dim lpCmd As Long = gchCmd.AddrOfPinnedObject().ToInt64()
        Dim lpdwRetLen As Long = gchCmd.AddrOfPinnedObject().ToInt64()
        CvGetCommand = CvGetCommand_(lPortNum, lpCmd, dwCmdLen, rb, dwRetBuffSize, lpdwRetLen)

        gchCmd.Free()
        gchRetLen.Free()

    End Function


    Public Function CvGetCommandEX( _
                ByVal lPortNum As Integer, _
                ByVal Cmd As String, _
                ByVal dwCmdLen As Integer, _
                ByRef rb As String, _
                ByVal dwRetBuffSize As Integer _
                ) As Integer

        Dim ByteArray As Byte()
        ByteArray = System.Text.Encoding.GetEncoding(65001).GetBytes(Cmd)
        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Long = gchCmd.AddrOfPinnedObject().ToInt64()
        CvGetCommandEX = CvGetCommandEX_(lPortNum, lpCmd, dwCmdLen, rb, dwRetBuffSize)

        gchCmd.Free()

    End Function


    Public Function SetCommand( _
                ByVal lPortNum As Integer, _
                ByVal Cmd As String, _
                ByVal dwCmdLen As Integer _
                ) As Integer
        SetCommand = CvSetCommand(lPortNum, Cmd, dwCmdLen)

    End Function


    Public Function GetCommand( _
                ByVal lPortNum As Integer, _
                ByVal Cmd As String, _
                ByVal dwCmdLen As Integer, _
                ByRef rb As String, _
                ByVal dwRetBuffSize As Integer, _
                ByRef dwRetLen As Integer _
                ) As Integer
        GetCommand = CvGetCommand(lPortNum, Cmd, dwCmdLen, rb, dwRetBuffSize, dwRetLen)

    End Function


    Public Function GetCommandEX( _
            ByVal lPortNum As Integer, _
            ByVal Cmd As String, _
            ByVal dwCmdLen As Integer, _
            ByRef rb As String, _
            ByVal dwRetBuffSize As Integer _
            ) As Integer

        GetCommandEX = CvGetCommandEX(lPortNum, Cmd, dwCmdLen, rb, dwRetBuffSize)

    End Function


    Public Function CvSetFirmwDataWrite( _
            ByVal lPortNum As Integer, _
            ByVal ByteArray() As Byte, _
            ByVal dwLen As Integer _
            ) As Integer

        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Long = gchCmd.AddrOfPinnedObject().ToInt64()
        CvSetFirmwDataWrite = CvSetFirmwDataWrite_(lPortNum, lpCmd, dwLen)

        gchCmd.Free()

    End Function


    Public Function SetFirmwDataWrite( _
            ByVal lPortNum As Integer, _
            ByVal ByteArray() As Byte, _
            ByVal dwLen As Integer _
            ) As Integer

        SetFirmwDataWrite = CvSetFirmwDataWrite(lPortNum, ByteArray, dwLen)

    End Function


    Public Function CvSetColorDataVersion( _
            ByVal lPortNum As Integer, _
            ByVal Cmd As String, _
            ByVal dwCmdLen As Integer _
            ) As Integer

        Dim ByteArray As Byte()
        ByteArray = System.Text.Encoding.GetEncoding(65001).GetBytes(Cmd)
        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Long = gchCmd.AddrOfPinnedObject().ToInt64()
        CvSetColorDataVersion = CvSetColorDataVersion_(lPortNum, lpCmd, dwCmdLen)

        gchCmd.Free()

    End Function


    Public Function SetColorDataVersion( _
            ByVal lPortNum As Integer, _
            ByVal Cmd As String, _
            ByVal dwCmdLen As Integer _
            ) As Integer

        SetColorDataVersion = CvSetColorDataVersion(lPortNum, Cmd, dwCmdLen)
    End Function


    Public Function CvSetColorDataWrite( _
            ByVal lPortNum As Integer, _
            ByVal ByteArray() As Byte, _
            ByVal dwLen As Integer _
            ) As Integer

        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Long = gchCmd.AddrOfPinnedObject().ToInt64()
        CvSetColorDataWrite = CvSetColorDataWrite_(lPortNum, lpCmd, dwLen)

        gchCmd.Free()

    End Function


    Public Function SetColorDataWrite( _
            ByVal lPortNum As Integer, _
            ByVal ByteArray() As Byte, _
            ByVal dwLen As Integer _
            ) As Integer

        SetColorDataWrite = CvSetColorDataWrite(lPortNum, ByteArray, dwLen)
    End Function


#Else

    Declare Function CvInitialize Lib "CxStat.dll" (<MarshalAs(UnmanagedType.LPWStr)> ByVal pszPortName As String) As Integer
    Declare Function CvGetVersion Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvGetSensorInfo Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvGetMedia Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvGetStatus Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetPQTY Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetCounterL Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetCounterA Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetCounterB Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetMediaCounter Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetMediaColorOffset Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetMediaLotNo Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvSetColorDataVersion_ Lib "CxStat.dll" Alias "CvSetColorDataVersion" (ByVal lPortNum As Integer, ByVal lpBuff As Integer, ByVal bLen As Integer) As Integer
    Declare Function CvSetColorDataWrite_ Lib "CxStat.dll" Alias "SetColorDataWrite" (ByVal lPortNum As Integer, ByVal lpBuff As Integer, ByVal bLen As Integer) As Integer
    Declare Function CvGetColorDataVersion Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvSetColorDataClear Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetColorDataChecksum Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvGetSerialNo Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function CvGetResolutionH Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetResolutionV Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvGetFreeBuffer Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvSetClearCounterA Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvSetClearCounterB Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvSetFirmwUpdateMode Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function CvSetFirmwDataWrite_ Lib "CxStat.dll" Alias "SetFirmwDataWrite" (ByVal lPortNum As Integer, ByVal lpBuff As Integer, ByVal bLen As Integer) As Integer
    Declare Function CvSetCommand_ Lib "CxStat.dll" Alias "CvSetCommand" (ByVal lPortNum As Integer, ByVal lpCmd As Integer, ByVal dwCmdLen As Integer) As Integer
    Declare Function CvGetCommand_ Lib "CxStat.dll" Alias "CvGetCommand" (ByVal lPortNum As Integer, ByVal lpCmd As Integer, ByVal dwCmdLen As Integer, ByVal lpRetBuff As String, ByVal dwRetBuffSize As Integer, ByRef lpdwRetLen As Integer) As Integer
    Declare Function CvGetCommandEX_ Lib "CxStat.dll" Alias "CvGetCommandEX" (ByVal lPortNum As Integer, ByVal lpCmd As Integer, ByVal dwCmdLen As Integer, ByVal lpRetBuff As String, ByVal dwRetBuffSize As Integer) As Integer
    '
    Declare Function PortInitialize Lib "CxStat.dll" (<MarshalAs(UnmanagedType.LPWStr)> ByVal pszPortName As String) As Integer
    Declare Function GetFirmwVersion Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetSensorInfo Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetMedia Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetStatus Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetPQTY Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterL Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterA Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterB Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterP Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterMatte Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetCounterM Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetMediaCounter Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetMediaCounter_R Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetMediaColorOffset Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetMediaLotNo Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetMediaIdSetInfo Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetColorDataVersion_ Lib "CxStat.dll" Alias "SetColorDataVersion" (ByVal lPortNum As Integer, ByVal lpBuff As Integer, ByVal bLen As Integer) As Integer
    Declare Function SetColorDataWrite_ Lib "CxStat.dll" Alias "SetColorDataWrite" (ByVal lPortNum As Integer, ByVal lpBuff As Integer, ByVal bLen As Integer) As Integer
    Declare Function GetColorDataVersion Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function SetColorDataClear Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetColorDataChecksum Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetSerialNo Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal s As String) As Integer
    Declare Function GetResolutionH Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetResolutionV Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function GetFreeBuffer Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetClearCounterA Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetClearCounterB Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetClearCounterM Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetCounterP Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal Counter As Integer) As Integer
    Declare Function SetFirmwUpdateMode Lib "CxStat.dll" (ByVal lPortNum As Integer) As Integer
    Declare Function SetFirmwDataWrite_ Lib "CxStat.dll" Alias "SetFirmwDataWrite" (ByVal lPortNum As Integer, ByVal lpBuff As Integer, ByVal bLen As Integer) As Integer
    Declare Function SetCutterMode Lib "CxStat.dll" (ByVal lPortNum As Integer, ByVal d As Integer) As Integer
    Declare Function SetCommand_ Lib "CxStat.dll" Alias "SetCommand" (ByVal lPortNum As Integer, ByVal lpCmd As Integer, ByVal dwCmdLen As Integer) As Integer
    Declare Function GetCommand_ Lib "CxStat.dll" Alias "GetCommand" (ByVal lPortNum As Integer, ByVal lpCmd As Integer, ByVal dwCmdLen As Integer, ByVal lpRetBuff As String, ByVal dwRetBuffSize As Integer, ByRef lpdwRetLen As Integer) As Integer
    Declare Function GetCommandEX_ Lib "CxStat.dll" Alias "GetCommandEX" (ByVal lPortNum As Integer, ByVal lpCmd As Integer, ByVal dwCmdLen As Integer, ByVal lpRetBuff As String, ByVal dwRetBuffSize As Integer) As Integer


    '-----------------------------------------------------------------------------------API Wrapper function
    Public Function CvSetCommand( _
            ByVal lPortNum As Integer, _
            ByVal Cmd As String, _
            ByVal dwCmdLen As Integer _
            ) As Integer

        Dim ByteArray As Byte()
        'Code Page 65001 = UTF-8
        ByteArray = System.Text.Encoding.GetEncoding(65001).GetBytes(Cmd)
        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Integer = gchCmd.AddrOfPinnedObject().ToInt32()
        CvSetCommand = CvSetCommand_(lPortNum, lpCmd, dwCmdLen)

        gchCmd.Free()

    End Function


    Public Function CvGetCommand( _
                    ByVal lPortNum As Integer, _
                    ByVal Cmd As String, _
                    ByVal dwCmdLen As Integer, _
                    ByRef rb As String, _
                    ByVal dwRetBuffSize As Integer, _
                    ByRef dwRetLen As Integer _
                    ) As Integer

        Dim ByteArray As Byte()
        ByteArray = System.Text.Encoding.GetEncoding(65001).GetBytes(Cmd)
        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim gchRetLen As GCHandle = GCHandle.Alloc(dwRetLen, GCHandleType.Pinned)
        Dim lpCmd As Integer = gchCmd.AddrOfPinnedObject().ToInt32()
        Dim lpdwRetLen As Integer = gchCmd.AddrOfPinnedObject().ToInt32()
        CvGetCommand = CvGetCommand_(lPortNum, lpCmd, dwCmdLen, rb, dwRetBuffSize, lpdwRetLen)

        gchCmd.Free()
        gchRetLen.Free()

    End Function


    Public Function CvGetCommandEX( _
                ByVal lPortNum As Integer, _
                ByVal Cmd As String, _
                ByVal dwCmdLen As Integer, _
                ByRef rb As String, _
                ByVal dwRetBuffSize As Integer _
                ) As Integer

        Dim ByteArray As Byte()
        ByteArray = System.Text.Encoding.GetEncoding(65001).GetBytes(Cmd)
        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Integer = gchCmd.AddrOfPinnedObject().ToInt32()
        CvGetCommandEX = CvGetCommandEX_(lPortNum, lpCmd, dwCmdLen, rb, dwRetBuffSize)

        gchCmd.Free()

    End Function


    Public Function SetCommand( _
                ByVal lPortNum As Integer, _
                ByVal Cmd As String, _
                ByVal dwCmdLen As Integer _
                ) As Integer
        SetCommand = CvSetCommand(lPortNum, Cmd, dwCmdLen)

    End Function


    Public Function GetCommand( _
                ByVal lPortNum As Integer, _
                ByVal Cmd As String, _
                ByVal dwCmdLen As Integer, _
                ByRef rb As String, _
                ByVal dwRetBuffSize As Integer, _
                ByRef dwRetLen As Integer _
                ) As Integer
        GetCommand = CvGetCommand(lPortNum, Cmd, dwCmdLen, rb, dwRetBuffSize, dwRetLen)

    End Function


    Public Function GetCommandEX( _
            ByVal lPortNum As Integer, _
            ByVal Cmd As String, _
            ByVal dwCmdLen As Integer, _
            ByRef rb As String, _
            ByVal dwRetBuffSize As Integer _
            ) As Integer

        GetCommandEX = CvGetCommandEX(lPortNum, Cmd, dwCmdLen, rb, dwRetBuffSize)

    End Function


    Public Function CvSetFirmwDataWrite( _
            ByVal lPortNum As Integer, _
            ByVal ByteArray() As Byte, _
            ByVal dwLen As Integer _
            ) As Integer

        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Integer = gchCmd.AddrOfPinnedObject().ToInt32()
        CvSetFirmwDataWrite = CvSetFirmwDataWrite_(lPortNum, lpCmd, dwLen)

        gchCmd.Free()

    End Function


    Public Function SetFirmwDataWrite( _
            ByVal lPortNum As Integer, _
            ByVal ByteArray() As Byte, _
            ByVal dwLen As Integer _
            ) As Integer

        SetFirmwDataWrite = CvSetFirmwDataWrite(lPortNum, ByteArray, dwLen)

    End Function


    Public Function CvSetColorDataVersion( _
            ByVal lPortNum As Integer, _
            ByVal Cmd As String, _
            ByVal dwCmdLen As Integer _
            ) As Integer

        Dim ByteArray As Byte()
        ByteArray = System.Text.Encoding.GetEncoding(65001).GetBytes(Cmd)
        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Integer = gchCmd.AddrOfPinnedObject().ToInt32()
        CvSetColorDataVersion = CvSetColorDataVersion_(lPortNum, lpCmd, dwCmdLen)

        gchCmd.Free()

    End Function


    Public Function SetColorDataVersion( _
            ByVal lPortNum As Integer, _
            ByVal Cmd As String, _
            ByVal dwCmdLen As Integer _
            ) As Integer

        SetColorDataVersion = CvSetColorDataVersion(lPortNum, Cmd, dwCmdLen)
    End Function


    Public Function CvSetColorDataWrite( _
            ByVal lPortNum As Integer, _
            ByVal ByteArray() As Byte, _
            ByVal dwLen As Integer _
            ) As Integer

        Dim gchCmd As GCHandle = GCHandle.Alloc(ByteArray, GCHandleType.Pinned)
        Dim lpCmd As Integer = gchCmd.AddrOfPinnedObject().ToInt32()
        CvSetColorDataWrite = CvSetColorDataWrite_(lPortNum, lpCmd, dwLen)

        gchCmd.Free()

    End Function


    Public Function SetColorDataWrite( _
            ByVal lPortNum As Integer, _
            ByVal ByteArray() As Byte, _
            ByVal dwLen As Integer _
            ) As Integer

        SetColorDataWrite = CvSetColorDataWrite(lPortNum, ByteArray, dwLen)
    End Function


#End If


End Module