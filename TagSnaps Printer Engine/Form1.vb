Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports Clockwork
Imports System.Net
Imports System.IO
Imports System.Collections.Concurrent
Imports System.Threading.Tasks
Imports System.Drawing.Printing


Friend Class Form1
    Inherits System.Windows.Forms.Form

    'Port Number
    Dim DS1 As Integer, DS2 As Integer, DS3 As Integer, DS4 As Integer

    'Printer device name
    Dim PDV1 As String, PDV2 As String, PDV3 As String, PDV4 As String

    'Printer active flag
    Dim p1 As Integer, p2 As Integer, p3 As Integer, p4 As Integer

    'The number of printer that can be used.
    Dim PrinterNum As Integer
    'Array of printer name that can be used.
    Dim Printers(4) As String

    Dim FileNum As Integer
    Dim FilePath As String

    Dim PrintQueue As New BlockingCollection(Of String)

    Dim cnt1 As Integer, cnt2 As Integer, cnt3 As Integer, cnt4 As Integer
    Dim cntrem1 As Integer, cntrem2 As Integer, cntrem3 As Integer, cntrem4 As Integer
    Dim fbuff1 As Integer, fbuff2 As Integer, fbuff3 As Integer, fbuff4 As Integer
    Dim status1 As String, status2 As String, status3 As String, status4 As String
    Dim Error1Msg As Boolean
    Dim watcher As FileSystemWatcher

    Dim Error1MsgFreq As Integer = 30

    Dim time1_err_tmp As Single, time2_err_tmp As Single, time3_err_tmp As Single, time4_err_tmp As Single
    Dim time1_err As Single, time2_err As Single, time3_err As Single, time4_err As Single

    Private Sub SendSMS(message As String)
        Try
            Dim api As Clockwork.API = New Clockwork.API("bbe5cd20faf50b9a33527f1ecbdf46ab17250e24")

            Dim sms As Clockwork.SMS = New Clockwork.SMS() With
                                       {.To = "61401235315",
                                        .Message = message
                                       }

            Dim result As SMSResult = api.Send(sms)

            If (result.Success) Then
                '          MessageBox.Show("Sent - ID: " + result.ID)
            Else
                '         MessageBox.Show("Error: " + result.ErrorMessage)
            End If

        Catch ex As APIException
            ' You'll get an API exception for errors 
            ' such as wrong key
            '      MessageBox.Show("API Exception: " + ex.Message)
        Catch ex As WebException
            'Web exceptions mean you couldn't reach the mediaburst server
            '     MessageBox.Show("Web Exception: " + ex.Message)
        Catch ex As ArgumentException
            ' Argument exceptions are thrown for missing parameters,
            ' such as forgetting to set the username
            '     MessageBox.Show("Argument Exception: " + ex.Message)
        Catch ex As Exception
            ' Something else went wrong, the error message should help
            '      MessageBox.Show("Unknown Exception: " + ex.Message)
        End Try

    End Sub

    Private Sub OnFormLoad(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        'Initialization of temporary for timer so that when it checks there is already an old time here
        time1_err_tmp = VB.Timer()
        time2_err_tmp = VB.Timer()
        time3_err_tmp = VB.Timer()
        time4_err_tmp = VB.Timer()

        FileNum = 0
        FilePath = "C:\TagSnaps\Photos\pending" 'Image data folder（Print from 00.JPG through 06.JPG of this folder repeatedly）
        TextBoxFolder.Text = FilePath

        ' Create a new FileSystemWatcher and set its properties. 
        watcher = New FileSystemWatcher() With
                  {.Path = FilePath,
                   .NotifyFilter = NotifyFilters.LastWrite Or NotifyFilters.FileName Or NotifyFilters.LastAccess,
                   .Filter = "*.jpg",
                   .IncludeSubdirectories = True
                  }

        ' Add event handlers. 
        AddHandler watcher.Created, AddressOf OnFolderChanged
        AddHandler watcher.Deleted, AddressOf OnFolderChanged

        ' Begin watching.
        watcher.EnableRaisingEvents = True

        'Set Printer Device Name
        PDV1 = "DS01"
        PDV2 = "DS02"
        PDV3 = "DS03"
        PDV4 = "DS04"

        'Get printer port number（Initialized status API）
        DS1 = PortInitialize("USB001")
        DS2 = PortInitialize("USB002")
        DS3 = PortInitialize("USB003")
        DS4 = PortInitialize("USB004")

        'Get counter value of each printer
        cnt1 = GetCounterL(DS1)
        cnt2 = GetCounterL(DS2)
        cnt3 = GetCounterL(DS3)
        cnt4 = GetCounterL(DS4)

        'Display count value
        If cnt1 >= 0 Then Text5.Text = Str(cnt1) Else Text5.Text = " ---"
        If cnt2 >= 0 Then Text6.Text = Str(cnt2) Else Text6.Text = " ---"
        If cnt3 >= 0 Then Text7.Text = Str(cnt3) Else Text7.Text = " ---"
        If cnt4 >= 0 Then Text8.Text = Str(cnt4) Else Text8.Text = " ---"

        'Get Sheets remaining of each printer
        cntrem1 = GetMediaCounter(DS1) - 50
        cntrem2 = GetMediaCounter(DS2) - 50
        cntrem3 = GetMediaCounter(DS3) - 50
        cntrem4 = GetMediaCounter(DS4) - 50

        'Display count value
        If cntrem1 >= 0 Then Text12.Text = Str(cntrem1) Else Text12.Text = " ---"
        If cntrem2 >= 0 Then Text13.Text = Str(cntrem2) Else Text13.Text = " ---"
        If cntrem3 >= 0 Then Text14.Text = Str(cntrem3) Else Text14.Text = " ---"
        If cntrem4 >= 0 Then Text15.Text = Str(cntrem4) Else Text15.Text = " ---"


        'Display each printer status on monitor
        Text1.Text = GetStateString(DS1)
        Text2.Text = GetStateString(DS2)
        Text3.Text = GetStateString(DS3)
        Text4.Text = GetStateString(DS4)

        'Printer search
        PrinterNum = 0
        p1 = PrinterSearch(PDV1, 0)
        p2 = PrinterSearch(PDV2, 1)
        p3 = PrinterSearch(PDV3, 2)
        p4 = PrinterSearch(PDV4, 3)

        'Clear of free buffer value
        fbuff1 = 0
        fbuff2 = 0
        fbuff3 = 0
        fbuff4 = 0

        'Set default value for the number of prints
        Text9.Text = CStr(1)
        '        Me.Image1.Image = Me.Frame1.BackgroundImage
        Me.Show()
        Task.Factory.StartNew(Sub() PrintQueueMonitor())
        'While True

        '    System.Windows.Forms.Application.DoEvents()

        '    'Get Sheets remaining of each printer
        '    'cntrem1 = GetMediaCounter(DS1)
        '    'cntrem2 = GetMediaCounter(DS2)
        '    'cntrem3 = GetMediaCounter(DS3)
        '    'cntrem4 = GetMediaCounter(DS4)

        '    'Display count value
        '    If cntrem1 >= 0 Then Text12.Text = Str(cntrem1) Else Text12.Text = " ---"
        '    If cntrem2 >= 0 Then Text13.Text = Str(cntrem2) Else Text13.Text = " ---"
        '    If cntrem3 >= 0 Then Text14.Text = Str(cntrem3) Else Text14.Text = " ---"
        '    If cntrem4 >= 0 Then Text15.Text = Str(cntrem4) Else Text15.Text = " ---"

        '    'status1 = GetStateStr(DS1) 'Get status
        '    'status2 = GetStateStr(DS2) 'Get status
        '    'status3 = GetStateStr(DS3) 'Get status
        '    'status4 = GetStateStr(DS4) 'Get status

        '    Text1.Text = status1 'Display the status
        '    Text2.Text = status2 'Display the status
        '    Text3.Text = status3 'Display the status
        '    Text4.Text = status4 'Display the status

        '    If status1 <> "IDLE" And status1 <> "PRINTING" Then 'Check error
        '        ToolStripStatusLabel1.Text = status1
        '        ToolStripStatusLabel1.BackColor = Color.DarkOrange

        '        If Not Error1Msg Or VB.Timer() - time1_err_tmp > Error1MsgFreq Then ' If error message is new or hasn't been seen for 30  seconds or current check freq
        '            '              MsgBox("Printer Error" & Chr(13) & Chr(10) & "[DS01] : " & status1, MsgBoxStyle.Critical)

        '            SendSMS(status1)
        '            Error1Msg = True
        '            Error1MsgFreq *= 2
        '            time1_err_tmp = VB.Timer()
        '        End If
        '    Else
        '        Error1Msg = False
        '        Error1MsgFreq = 30
        '        ToolStripStatusLabel1.Text = "All Good"
        '        ToolStripStatusLabel1.BackColor = Color.Empty
        '    End If


        '    Print_Click()
        '    Sleep(10000)


        'End While



    End Sub

    ' Define the event handlers. 
    Private Sub OnFolderChanged(source As Object, e As FileSystemEventArgs)
        Select Case e.ChangeType
            Case WatcherChangeTypes.Created
                PrintQueue.Add(e.FullPath)
        End Select

    End Sub
    'Printer search
    Function PrinterSearch(ByVal PDV As String, ByVal n As Integer) As Integer
        For Each strprinter As String In Printing.PrinterSettings.InstalledPrinters
            If strprinter = PDV Then
                Printers(PrinterNum + 1) = PDV
                PrinterNum = PrinterNum + 1
                Exit For
            End If
        Next
        PrinterSearch = PrinterNum

    End Function

    Private Sub MoveFiles()
        Dim testFile As System.IO.FileInfo
        Dim fileName As String
        Dim Archive As Boolean = True

        '        Me.Hide()
        Me.Image1.Dispose()
        '        Me.Dispose()


        System.Windows.Forms.Application.DoEvents()

        For Each foundfile As String In Me.PrintQueue
            fileName = "None"
            Try
                testFile = My.Computer.FileSystem.GetFileInfo(foundfile)
                fileName = testFile.Name
                If CheckBox1.CheckState() Then
                    My.Computer.FileSystem.CopyFile(foundfile, (FilePath & "\Archive\" & fileName), True)
                End If
                My.Computer.FileSystem.DeleteFile(foundfile)
            Catch IOExcep As System.IO.IOException
                MsgBox("File transfer Error" & Chr(13) & Chr(10) & fileName & IOExcep.Message & Chr(13) & Chr(10), MsgBoxStyle.Critical)
                Console.WriteLine(IOExcep.Message)
            End Try
        Next

    End Sub

    Private Sub PrintPhoto(filePath As String)

        Dim printing As Boolean
        Dim i As Integer
        Dim DSNo As Integer

        Dim fbuff1 As Integer, fbuff2 As Integer, fbuff3 As Integer, fbuff4 As Integer
        'Printer count value
        Dim cnt1 As Integer, cnt2 As Integer, cnt3 As Integer, cnt4 As Integer
        'Application count value
        Dim cnt1_tmp As Integer, cnt2_tmp As Integer, cnt3_tmp As Integer, cnt4_tmp As Integer



        Dim pJob_Renamed As Boolean 'Print job checking flag
        Dim fopen_flg As Byte 'Flag for the file already opened
        Dim status As String
        Dim sheetsRemaining As Integer

        Dim time01 As Single, time02 As Single, time03 As Single, time04 As Single

        'Variable for measurement at time
        Dim time01_tmp As Single, time02_tmp As Single, time03_tmp As Single, time04_tmp As Single
        Dim free_buff01 As Integer, free_buff02 As Integer, free_buff03 As Integer, free_buff04 As Integer

        'Get count value of each printer
        cnt1 = GetCounterL(DS1)
        cnt2 = GetCounterL(DS2)
        cnt3 = GetCounterL(DS3)
        cnt4 = GetCounterL(DS4)

        'Display count value of each printer
        Me.Invoke(Sub()
                      If cnt1 >= 0 Then Text5.Text = Str(cnt1) Else Text5.Text = " ---"
                      If cnt2 >= 0 Then Text6.Text = Str(cnt2) Else Text6.Text = " ---"
                      If cnt3 >= 0 Then Text7.Text = Str(cnt3) Else Text7.Text = " ---"
                      If cnt4 >= 0 Then Text8.Text = Str(cnt4) Else Text8.Text = " ---"
                  End Sub)


        'Set application count value to match printer count value
        cnt1_tmp = cnt1
        cnt2_tmp = cnt2
        cnt3_tmp = cnt3
        cnt4_tmp = cnt4


        'Set current printer to printer #1
        DSNo = 1

        'Initialization of temporary for timer
        time01_tmp = VB.Timer()
        time02_tmp = VB.Timer()
        time03_tmp = VB.Timer()
        time04_tmp = VB.Timer()

        OpenFile(filePath) 'Open image file

        While Not printing
            Me.Invoke(Sub()
                          If DSNo = 1 Then
                              sheetsRemaining = GetMediaCounter(DS1)
                              If sheetsRemaining >= 0 Then Text12.Text = Str(sheetsRemaining) Else Text12.Text = " ---"
                              pJob_Renamed = CheckPrnJob(PDV1) 'Check print job
                              If pJob_Renamed = False Then 'If there is no print job, check count value
                                  status = GetStateString(DS1) 'Get status
                                  Text1.Text = status 'Display the status
                                  If status <> "IDLE" And status <> "PRINTING" And status <> " ---" Then 'Check error
                                      cnt1 = GetCounterL(DS1) 'Get count value
                                      If cnt1 >= 0 Then Text5.Text = Str(cnt1) Else Text5.Text = " ---" 'Display count value
                                      cnt1_tmp = cnt1 'If error is detected, initialize count value
                                      MsgBox("Printer Error" & Chr(13) & Chr(10) & "[DS01] : " & status, MsgBoxStyle.Critical)
                                      '**************
                                      'Error Recovery
                                      '**************
                                      '
                                      '
                                      '
                                  Else
                                      cnt1 = GetCounterL(DS1) 'Get count value
                                      If cnt1 >= 0 Then Text5.Text = Str(cnt1) Else Text5.Text = " ---" 'Display count value
                                      '                    If (cnt1 >= 0) And (cnt1_tmp - cnt1 <= 1) Then
                                      fbuff1 = GetFreeBuffer(DS1)
                                      If (fbuff1 >= 1) Then
                                          status = GetStateString(DS1) 'Get status
                                          Text1.Text = status 'Display the status
                                          If status = "IDLE" Or status = "PRINTING" Then 'Check error
                                              i = p1 'There is an empty buffer of DS01
                                          End If
                                      End If
                                  End If
                                  time01_tmp = VB.Timer()
                              Else
                                  time01 = VB.Timer() - time01_tmp 'Check waiting time of the print job in print queue.
                                  If time01 > 60 Then 'Error process if there is a job which stays more than 1 minute.
                                      MsgBox("Please check Print Job" & Chr(13) & Chr(10) & "[DS01]", MsgBoxStyle.Critical)
                                      '**************
                                      'Error Recovery
                                      '**************
                                      '
                                      '
                                      '
                                      Text11.Text = String.Empty
                                  End If
                              End If
                              If sheetsRemaining < 0 Then
                                  DSNo = 2 'Change current printer
                              End If


                              '========
                              '  DS02
                              '========
                          ElseIf DSNo = 2 Then
                              sheetsRemaining = GetMediaCounter(DS2)
                              If sheetsRemaining >= 0 Then Text13.Text = Str(sheetsRemaining) Else Text13.Text = " ---"
                              pJob_Renamed = CheckPrnJob(PDV2) 'Check print job
                              If pJob_Renamed = False Then 'If there is no print job, check count value
                                  status = GetStateString(DS2) 'Get status
                                  Text2.Text = status 'Display the status
                                  If status <> "IDLE" And status <> "PRINTING" And status <> " ---" Then 'Check error
                                      cnt2 = GetCounterL(DS2) 'Get count value
                                      If cnt2 >= 0 Then Text6.Text = Str(cnt2) Else Text6.Text = " ---" 'Display count value
                                      cnt2_tmp = cnt2 'If error is detected, initialize count value 
                                      MsgBox("Printer Error" & Chr(13) & Chr(10) & "[DS02] : " & status, MsgBoxStyle.Critical)
                                      '**************
                                      'Error Recovery
                                      '**************
                                      '
                                      '
                                      '
                                  Else
                                      cnt2 = GetCounterL(DS2) 'Get count value
                                      If cnt2 >= 0 Then Text6.Text = Str(cnt2) Else Text6.Text = " ---" 'Display count value
                                      '                    If (cnt2 >= 0) And (cnt2_tmp - cnt2 <= 1) Then
                                      fbuff2 = GetFreeBuffer(DS2)
                                      If (fbuff2 >= 1) Then
                                          status = GetStateString(DS2) 'Get status
                                          Text2.Text = status 'Display the status
                                          If status = "IDLE" Or status = "PRINTING" Then 'Check error
                                              i = p2 'There is empty buffer of DS02
                                          End If
                                      End If
                                  End If
                                  time02_tmp = VB.Timer()
                              Else
                                  time02 = VB.Timer() - time02_tmp 'Check waiting time of the print job in print queue.
                                  If time02 > 60 Then 'Error process if there is a job which stays more than 1 minute.
                                      MsgBox("Please check Print Job" & Chr(13) & Chr(10) & "[DS02]", MsgBoxStyle.Critical)
                                      '**************
                                      'Error Recovery
                                      '**************
                                      '
                                      '
                                      '
                                      Text11.Text = String.Empty
                                  End If
                              End If
                              If sheetsRemaining < 0 Then
                                  DSNo = 2 'Change current printer
                              End If

                              '========
                              '  DS03
                              '========
                          ElseIf DSNo = 3 Then
                              sheetsRemaining = GetMediaCounter(DS3)
                              If sheetsRemaining >= 0 Then Text14.Text = Str(sheetsRemaining) Else Text14.Text = " ---"
                              pJob_Renamed = CheckPrnJob(PDV3) 'Check print job
                              If pJob_Renamed = False Then 'If there is no print job, check count value 
                                  status = GetStateString(DS3) 'Get status
                                  Text3.Text = status 'Display the status
                                  If status <> "IDLE" And status <> "PRINTING" And status <> " ---" Then 'Check error
                                      cnt3 = GetCounterL(DS3) 'Get coutn value
                                      If cnt3 >= 0 Then Text7.Text = Str(cnt3) Else Text7.Text = " ---" 'Display count value
                                      cnt3_tmp = cnt3 'If error is detected, initialize count value
                                      MsgBox("Printer Error" & Chr(13) & Chr(10) & "[DS03] : " & status, MsgBoxStyle.Critical)
                                      '**************
                                      'Error Recovery
                                      '**************
                                      '
                                      '
                                      '
                                  Else
                                      cnt3 = GetCounterL(DS3) 'Get count value
                                      If cnt3 >= 0 Then Text7.Text = Str(cnt3) Else Text7.Text = " ---" 'Displayy count value
                                      '                    If (cnt3 >= 0) And (cnt3_tmp - cnt3 <= 1) Then
                                      fbuff3 = GetFreeBuffer(DS3)
                                      If (fbuff3 >= 1) Then
                                          status = GetStateString(DS3) 'Get status
                                          Text3.Text = status 'Display the status
                                          If status = "IDLE" Or status = "PRINTING" Then 'Check error
                                              i = p3 'There is empty bufffer of DS03
                                          End If
                                      End If
                                  End If
                                  time03_tmp = VB.Timer()
                              Else
                                  time03 = VB.Timer() - time03_tmp 'Check waiting time of the print job in print queue.
                                  If time03 > 60 Then 'Error process if there is a job which stays more than 1 minute.
                                      MsgBox("Please check Print Job" & Chr(13) & Chr(10) & "[DS03]", MsgBoxStyle.Critical)
                                      '**************
                                      'Error Recovery
                                      '**************
                                      '
                                      '
                                      '
                                      Text11.Text = String.Empty
                                  End If
                              End If
                              If sheetsRemaining < 0 Then
                                  DSNo = 2 'Change current printer
                              End If

                              '========
                              '  DS04
                              '========
                          Else
                              sheetsRemaining = GetMediaCounter(DS4)
                              If sheetsRemaining >= 0 Then Text15.Text = Str(sheetsRemaining) Else Text15.Text = " ---"
                              pJob_Renamed = CheckPrnJob(PDV4) 'Check print job
                              If pJob_Renamed = False Then 'If there is no print job, check count value
                                  status = GetStateString(DS4) 'Get status
                                  Text4.Text = status 'Display the status
                                  If status <> "IDLE" And status <> "PRINTING" And status <> " ---" Then 'Check error
                                      cnt4 = GetCounterL(DS4) 'Get count value
                                      If cnt4 >= 0 Then Text8.Text = Str(cnt4) Else Text8.Text = " ---" 'Display count value
                                      cnt4_tmp = cnt4 'If error is detected, initialize count value
                                      MsgBox("Printer Error" & Chr(13) & Chr(10) & "[DS04] : " & status, MsgBoxStyle.Critical)
                                      '**************
                                      'Error Recovery
                                      '**************
                                      '
                                      '
                                      '
                                  Else
                                      cnt4 = GetCounterL(DS4) 'Get count value
                                      If cnt4 >= 0 Then Text8.Text = Str(cnt4) Else Text8.Text = " ---" 'Display count value
                                      '                    If (cnt4 >= 0) And (cnt4_tmp - cnt4 <= 1) Then
                                      fbuff4 = GetFreeBuffer(DS4)
                                      If (fbuff4 >= 1) Then
                                          status = GetStateString(DS4) 'Get status
                                          Text4.Text = status 'Display the status
                                          If status = "IDLE" Or status = "PRINTING" Then 'Check error
                                              i = p4 'There is empty buffer of DS04
                                          End If
                                      End If
                                  End If
                                  time04_tmp = VB.Timer()
                              Else
                                  time04 = VB.Timer() - time04_tmp 'Check waiting time of the print job in print queue.
                                  If time04 > 60 Then 'Error process if there is a job which stays more than 1 minute.
                                      MsgBox("Please check Print Job" & Chr(13) & Chr(10) & "[DS04]", MsgBoxStyle.Critical)
                                      '**************
                                      'Error Recovery
                                      '**************
                                      '
                                      '
                                      '
                                      Text11.Text = String.Empty
                                  End If
                              End If
                              If sheetsRemaining < 0 Then
                                  'throw new error no more paper
                              End If

                          End If
                      End Sub)

            'Print process
            If i > 0 Then 'Is there any printer available to send data?
                Dim pd As New PrintDocument
                AddHandler pd.PrintPage, AddressOf OnPrintDocumentPrintPage
                pd.PrinterSettings.PrinterName = Printers(i)
                pd.Print()
                pd.Dispose()
                printing = True
                'Count up the counter at application side
                If i = p1 Then cnt1_tmp = cnt1_tmp + 1
                If i = p2 Then cnt2_tmp = cnt2_tmp + 1
                If i = p3 Then cnt3_tmp = cnt3_tmp + 1
                If i = p4 Then cnt4_tmp = cnt4_tmp + 1

                fopen_flg = 0

            Else
                'Process if there is no available printer to send data
                Task.Delay(100) 'If all printers are busy, take count value at 100ms intervals
            End If

        End While

        'Check completion
        Do
            free_buff01 = GetFreeBuffer(DS1)
            free_buff02 = GetFreeBuffer(DS2)
            free_buff03 = GetFreeBuffer(DS3)
            free_buff04 = GetFreeBuffer(DS4)

            If (free_buff01 = 2 Or free_buff01 = -1) And (free_buff02 = 2 Or free_buff02 = -1) And (free_buff03 = 2 Or free_buff03 = -1) And (free_buff04 = 2 Or free_buff04 = -1) Then
                Me.Invoke(Sub()
                              Text1.Text = GetStateString(DS1) 'Get status
                              Text2.Text = GetStateString(DS2) 'Get status
                              Text3.Text = GetStateString(DS3) 'Get status
                              Text4.Text = GetStateString(DS4) 'Get status
                          End Sub)
                Exit Do
            End If
            Task.Delay(100)
        Loop

        Me.Invoke(Sub()
                      Me.Image1.Visible = False
                  End Sub)


        MoveFiles()

    End Sub

    Private Sub Form1_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        With Image1

            .Top = 0
            .Left = 0
            .Width = Frame1.Width
            .Height = Frame1.Height

        End With

    End Sub

    Private Sub OpenFile(filePath As String)

        Using fileStream As New FileStream(filePath, FileMode.Open, FileAccess.Read)
            If Me.InvokeRequired Then
                Me.Invoke(Sub()
                              Me.Image1.Image = System.Drawing.Image.FromStream(fileStream)
                              Me.Image1.Visible = True
                          End Sub)
                Return
            End If
            Me.Image1.Image = System.Drawing.Image.FromStream(fileStream)
            Me.Image1.Visible = True
        End Using

        Exit Sub
    End Sub

    Private Sub OnPrintDocumentPrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)

        Dim image As Image = Image1.Image

        If image.Width < image.Height And e.PageBounds.Width > e.PageBounds.Height Then
            image.RotateFlip(RotateFlipType.Rotate90FlipNone)
        End If

        'Output size calucration（Fit width with paper size, and calculate vertical size according to the ratio）
        Dim Print_Width As Single = e.PageBounds.Width
        Dim Ratio_Width As Single = e.PageBounds.Width / image.Width
        Dim Print_Height As Single = image.Height * Ratio_Width


        e.Graphics.DrawImage(image, 0, 0, Print_Width, Print_Height)
        e.HasMorePages = False

    End Sub

    'When the printer status is recieved, the character string of status is returned. 
    Function GetStateString(ByVal UsbPort As Integer) As String
        Dim state As Long
        Dim stateStr As String

        stateStr = ""

        state = GetStatus(UsbPort)
        If state And GROUP_USUALLY Then '================ Usually error status group
            Select Case state
                Case STATUS_USUALLY_IDLE : stateStr = "IDLE"
                Case STATUS_USUALLY_PRINTING : stateStr = "PRINTING"
                Case STATUS_USUALLY_STANDSTILL : stateStr = "STANDSTILL"
                Case STATUS_USUALLY_PAPER_END : stateStr = "PAPER_END"
                Case STATUS_USUALLY_RIBBON_END : stateStr = "RIBBON_END"
                Case STATUS_USUALLY_COOLING : stateStr = "COOLING"
            End Select
        ElseIf state And GROUP_SETTING Then '============ Printer setting status group
            Select Case state
                Case STATUS_SETTING_COVER_OPEN : stateStr = "COVER_OPEN"
                Case STATUS_SETTING_PAPER_JAM : stateStr = "PAPER_JAM"
                Case STATUS_SETTING_RIBBON_ERR : stateStr = "RIBBON_ERR"
                Case STATUS_SETTING_PAPER_ERR : stateStr = "PAPER_ERR"
                Case STATUS_SETTING_DATA_ERR : stateStr = "DATA_ERR"
            End Select
        ElseIf state And GROUP_HARDWARE Then '=========== Hardware error status group
            Select Case state
                Case STATUS_HARDWARE_ERR01 : stateStr = "Head Voltage Err"
                Case STATUS_HARDWARE_ERR02 : stateStr = "Head Position Err"
                Case STATUS_HARDWARE_ERR03 : stateStr = "Fan Stop Err"
                Case STATUS_HARDWARE_ERR04 : stateStr = "Cutter Err"
                Case STATUS_HARDWARE_ERR05 : stateStr = "Pinch Roller Err"
                Case STATUS_HARDWARE_ERR06 : stateStr = "Illegal Head Temp."
                Case STATUS_HARDWARE_ERR07 : stateStr = "Illegal Media Temp."
                Case STATUS_HARDWARE_ERR08 : stateStr = "Ribbon Tension Err"
                Case STATUS_HARDWARE_ERR09 : stateStr = "RFID Module Err"
            End Select
        ElseIf state And GROUP_SYSTEM Then '============= System error status group
            stateStr = "SYSTEM ERROR"
        ElseIf state And GROUP_FLSHPROG Then '=========== FLASH ROM rewriting status group
            stateStr = "FLSHPROG MODE"
        ElseIf state < 0 Then
            stateStr = " ---"
        End If

        GetStateString = stateStr

    End Function

    Private Sub PrintQueueMonitor()
        While PrintQueue.IsCompleted = False
            Dim filePath As String = Nothing
            Try
                filePath = PrintQueue.Take()
            Catch e As InvalidOperationException
                ' IOE means that Take() was called on a completed collection. 
                ' In this example, we can simply catch the exception since the  
                ' loop will break on the next iteration. 
            End Try
            If (filePath IsNot Nothing) Then
                PrintPhoto(filePath)
            End If
        End While
    End Sub

    Private Sub Command1_Click(sender As Object, e As EventArgs) Handles Command1.Click

    End Sub
End Class